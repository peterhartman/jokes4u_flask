from flask import Flask, render_template
import random

app = Flask(__name__)
jokes = [
    {'front': 'What is brown and sticky?', 'back': 'A stick!'},
    {'front': 'How do you make one go away?', 'back': 'Add a G and then it is GONE!'},
    {'front': 'What should you do if you find a green alien?', 'back': 'Wait till its ripe!'},
    {'front': 'What did the hungry alien say when it landed on earth?', 'back': 'Take me to your larder!'},
    {'front': 'Wanna know a word I just made up?', 'back': 'Plagiarism.'},
    {'front': 'What do you call a witch that lives in the beach?', 'back': 'A sandwitch!'},
    {'front': 'What’s the best thing about Switzerland?', 'back': 'I don’t know but their flag is a big plus!'},
    {'front': 'Why can\'t you trust stairs?', 'back': 'Because they are always up to something.'},
    ]

@app.route('/')
@app.route('/index.html')
def hello_world():
    return render_template('joke.html', joke=random.choice(jokes))